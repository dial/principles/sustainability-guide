# What does the Toolkit aim to achieve?

The goal of the Business Model Sustainability (BMS) Toolkit is to enable intrapreneurs and entrepreneurs in any corner of the world, who have developed digital solutions to be used for development or humanitarian purposes, to create a sustainable business model to sustain the impact of the solution. 

The path to sustainability is one that can be forged in different ways. These may include: 

- Forming trusted partnerships and capitalizing on collaboration, 
- Achieving sustainable revenue generation, or 
- Developing a transition plan to shift costs to the end game provider. 

An organisation utilizing the BMS Toolkit will better understand how it can generate, deliver and capture value in a sustainable way – and in turn realize its economic, social and/or environmental impact goals. It will be able to develop business models that will enable the sustainable and scalable use of digital programs, tools or products to significantly impact development and/or humanitarian outcomes. 
