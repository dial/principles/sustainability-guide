# What is the Business Model Sustainability Toolkit?

The Business Model Sustainability (BMS) Toolkit – sponsored by Fondation Botnar in partnership with the Digital Impact Alliance (DIAL) – helps social enterprises, NGOs and small businesses think about their own sustainability, and the sustainability of their digital solutions. It consists of a business model diagnostic tool, a Business Model Sustainability Canvas, and a Business Model Sustainability Guide — which includes instructive guidance, case studies and interactive tools for users.

Focusing on the principle of [Build for Sustainability](https://digitalprinciples.org/principle/build-for-sustainability/) – a key consideration to ensure long-term success of a digital solution – the BMS Toolkit will help digital development practitioners build their confidence and capacity to apply it to their work. When developing a sustainable digital solution, it is important to understand and embody the foundational tenets of [Build for Sustainability](https://digitalprinciples.org/principle/build-for-sustainability/), which include:

- Maintaining user and stakeholder support, as well as maximizing long-term impact,
- Ensuring user and stakeholder contributions are not minimized due to interruptions, such as a loss of funding, and
- Creating solutions that are more likely to be embedded into policies, daily practices and user workflow.
