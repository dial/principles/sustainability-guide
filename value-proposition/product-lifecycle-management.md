---
related_topics: ['revenue-streams', 'key-activities', 'end-game', 'cost-structure']
---

# Product Lifecycle Management

## Elements and Stages of Product Lifecycle Management

Many digital products in the aid sector remain in a state of perpetual pilot as they struggle to access sustained funding or revenue. They’re unable to generate sufficient surplus to invest in their own organisation or the maintenance and future releases of their software. This becomes doubly difficult if the solution is a combination of software and hardware. In order to create both a financially and environmentally sustainable (for hardware) solution, you need to map out the full lifecycle of your digital solution, establish what happens over the lifecycle, and define who is responsible for this.

:::note Key Resource

For additional information about product lifecycle management, see Smartsheet’s [PLM Guide](https://www.smartsheet.com/product-life-cycle-management).

:::

The three main elements to think about in regards to product lifecycle management (PLM) are:

- **Software** – Including architecture and platforms, and possibly cloud-based or locally installed platforms
- **Processes** – Such as training, deployments, and adoption 
- **Methods** – Codified systems and rules

Think about each of these elements across the five stages of the product lifecycle:

- Introduction
- Growth
- Maturity 
- Decline
- End of life

Your product lifecycle links with your [end game](../end-game/overview.md) and is critical to identifying key stakeholders along the journey. For instance, innovation funders will provide money for the introduction of a solution, but you will need to find other funders for the latter phases of the growth stage and maturity stage. 

Your product lifecycle is not only important for environmental and financial sustainability, but also to ensure users and stakeholders continue to gain value from your digital solution across the lifecycle. This is the case even at the end of life of your solution, when the users’ needs still remain. You’ll have to consider whether users become unsupported or something else will be put in place.

Aspects of your digital solution to think about across the product lifecycle are contextual appropriateness, maintenance and updates, technical support, and e-waste.

## Contextual Appropriateness

When designing your solution, you need to think about where it’s likely to be used. You can do so by asking: Was the solution designed with the local ecosystem and user in mind?. For example, do you want it to be used to address a problem in a low-resource context? Do you want it used in multiple different low-resource contexts as you scale? You’ll need to think through the following contextual questions:

1. **Hardware and software**: If your digital solution is designed for a particular piece of hardware, what is the likely longevity, usability, and availability of parts and servicing for this hardware? What is the price of the hardware in that context? Is the device robust enough for that context? Or is the digital solution a cloud-based platform? If so, how are questions of data security addressed?
2. **Connectivity and power**: Is there network coverage? Is there sufficient mobile data connectivity, and if there is, is it affordable? Is there access to electricity supply?
3. **Compatibility**: Is your software compatible with the most used and/or cheapest operating system that digital devices are running on in that context? Are there skills in the country to support the product?
4. **Users and target impact segments**: Is your solution contextually appropriate and accessible for the users and people whom you are seeking to have an impact on? Did you create your solution with the [Design With The User](https://digitalprinciples.org/principle/design-with-the-user/) Principle in mind?


:::note Key Resource

For other questions regarding contextual appropriateness, use DIAL’s [external considerations](https://digitalimpactalliance.org/wp-content/uploads/2021/01/Sustainability-Guidbook.pdf) exercise (p.25)

:::

## Maintenance and Updates

Ongoing maintenance of your solution is key, ensuring that it continues to be secure and that Application Programming Interfaces (APIs) continue to function. The **three main questions** to ask are:

- How will maintenance and updates happen? 
- Who will be responsible for them? 
- How will they be funded?

In the aid sector, particularly when funded by grants, having a long-term plan for maintenance and updates can be difficult. Ensure that you factor them into funding applications. Your product will not be sustainable unless this area has a strong, actionable plan.

## Technical Support

Ensuring that there is sufficient technical support available for users at all times is critical for the sustainability of your solution. Assess whether local talent may serve as technical support. If you cannot provide good technical support and fixes, users will not continue to use your digital solution. The following are the basic levels of IT support:

- **Tier 0**: The user self-directs their support using online tutorials, videos, and instructions.
- **Tier 1**: General basic support is provided by a person or bot.
- **Tier 2**: In-depth support is provided by a skilled technician.
- **Tier 3**: Specialised technical support with appropriate tiering is critical for users.

:::note Key Resource

For more information on technical support, see [IT Support Levels Clearly Explained: T1, T2, T3, and More](https://www.bmc.com/blogs/support-levels-level-1-level-2-level-3/).

:::

All technical support should cover these tiers in some way. The **three main questions** to ask are:

- Which tiers do we in-source (i.e., do ourselves)?
- Which tiers do we outsource (i.e., pay someone else to do)?
- Which tiers do we crowdsource (i.e., use peer-to-peer free support through a user community)?

These decisions can change over time, particularly if you grow a large community of users or if you scale your solution. Therefore, it’s wise to review your approach to technical support regularly to ensure it’s sustainable.

## E-Waste

If your digital solution combines software with hardware, such as tablets, sensors, or other internet of things (IoT) devices, then you should factor in how the end of life of the hardware and batteries will be managed. The three main questions are: 

- Who collects them? 
- How are they recycled? 
- If they need to be destroyed, is this done in the cleanest possible way? 

E-waste is a growing problem in many low- and middle-income countries. You will need to ensure that you are not adding to this problem. 

:::tip  Case Study

Fairfone rethinks the way phones are made as it seeks to create a circular economy. It does this by encouraging the reuse and repair of our phones, researching electronics recycling options, and reducing electronic waste worldwide. It has created a modular and repairable design that is rare in the electronics industry.  

Fairfone’s approach includes rethinking its value chain, including mining, design, and manufacturing.  It uses fair-trade gold, recycled plastics, and conflict-free minerals, and it offers a recycling programme for all phones to incentivise recycling and ensure valuable materials can be used in a continuous loop. The company even sells spare parts and offers repair tutorials to help make phones useful for as long as possible.  

While Fairfone’s processes are not yet perfect, they are further advanced in a circular process than others. 

:::

## Key Takeaways

- Product lifecycle management is a critical part of sustainability.
- Before seeking to deploy your digital solution at any significant 
scale, you need to ask yourself the questions in this section regarding how it was created, if it keeps the user and local ecosystem in mind, and map out your answers for each of them.
- Ensure that you have an e-waste strategy developed before introducing any hardware as part of your solution.

## Complete the following in your Canvas

- Describe how your product will continue to maintain its value for users by assessing your product lifecycle management.
