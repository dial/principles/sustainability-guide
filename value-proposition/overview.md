---
relevant_principles: ['design-with-user', 'understand-existing-ecosystem', 'design-for-scale', 'building-for-sustainability', 'use-open-standards']
related_topics: ['revenue-streams', 'key-activities', 'customer-relationships']
---

# Value Proposition

Digital products and services do not stand alone. Rather, they’re integrated into systems and processes such as training, teaching, and data collection activities. We call this combination of digital products or services and their context a digital solution.

:::note Key Resources

- [The Value Proposition Canvas](https://www.strategyzer.com/canvas/value-proposition-canvas) is a tool created by Strategyzer, which turns the [“Jobs to be Done” methodology](https://www.youtube.com/watch?v=sfGtw2C95Ms) into a practical method for assessing your value proposition in relation to your customers’ needs.
- Video: [Value Proposition Canvas Explained](https://www.strategyzer.com/canvas/value-proposition-canvas)

:::

A solution needs a compelling value proposition for those stakeholders who will interact with it, including buyers (those who are part of the purchasing process), users (those who physically use the product) and target impact segments (those who are experiencing the problem that the solution seeks to resolve).[^1]

A value proposition can be defined as a combination of goods and services that provide value to buyers, users, and target impact segments.

When developing a solution, it should create immediate value for buyers, users, and target impact segments. Additionally, the business model underpinning it should be financially sustainable. Building sustainable business models to deliver solutions enables long-term success by ensuring the solution is supported at all stages of its lifecycle.

To complete the value proposition building block in your Business Model Canvas, there are three key aspects that need to be unpacked. In section 1.1 and 1.2 there is guidance on ensuring the value proposition and digital solution you have developed for the aid sector is sustainable, and in section 1.3 there is information on the need to measure impact to ensure your solution provides a long-term public benefit.

## Modules in this Building Block

### Product Lifecycle Management

This section provides guidance on developing a full lifecycle for the solution and understanding how it fits in with the “clockspeed” of the aid sector. The clockspeed is the timing and sequencing of core processes in the sector, such as the longevity of big INGOs, short funding cycles, and the impact of policy review cycles.

#### Key discussion areas:

- Map the full lifecycle of the solution and establish what happens over the lifecycle
- Understand the solution’s contextual appropriateness, maintenance and updates, technical support, and e-waste

### Adaptability

This section provides guidance on establishing what parts of the solution are core and remain the same in every instance; what parts are modular, where the user can choose from options you supply; and what parts are hackable, where the solution can be changed by the user.

#### Key discussion areas: 

- Identify the parts of the solution that are core, modular and hackable

### Replicability

This section provides guidance on documenting and/or automating of systems, processes, rules, decisions, and intangible knowledge that are needed to make your solution replicable and implementable by anybody. Ensuring that all parts of your solution are codified will make it easily replicable.

#### Key discussion areas:

- Codify the key components of the solution

### Evidence of Impact

This section provides a short overview on social return on investment (SROI), and the need to ensure the solution actually delivers a positive impact for the target impact segment.

#### Key discussion areas:

- Understand the importance of measuring impact, and why using SROI is a good method

:::tip Case Study: Illustration of a value proposition for a digital solution

A value proposition needs to clearly tell the world what value the digital solution creates. A good example is this one from [Viamo](https://viamo.io/):

> Viamo provides low-cost mobile engagement solutions in landscapes where technology infrastructure is poor, populations are divided by language diversity, and where education and literacy levels are low.

:::

## Key Takeaways

1. Sustainability must be embedded in the solution and the value proposition. Mapping out and creating a sustainable lifecycle for the digital product or service will help achieve this.
2. The solution needs to be appropriate to the local ecosystem yet able to scale beyond where it is piloted. Establishing what is core, modular, and hackable will help with this. It can also support the identification of what parts of the solution could potentially earn revenue to increase the sustainability of the business model.
3. Understanding how to measure the social impact of the solution is 
key to showing the value proposition for buyers, users, and target impact segments. 

## Complete the following in your Business Model Canvas

- Articulate a succinct value proposition for buyers, users, and target impact segments.
- Highlight the sustainability of the value proposition using the product lifecycle exercise and its social impact using the evidence generation and SROI guidance.


[^1]: For more on buyers, users, and target impact segments, see the [Customer Segments](../customer-segments/) building block.
