# Sustainabilty Guide

Welcome to the content repository for the DIAL Business Model Sustainability Guide.

## Contributing

_Note: This section will be expanded much more fully._

We welcome suggested changes or additions to the Guide! There are three ways you can recommend or request edits:

- **Fork the project, make the edit, and open a merge request.** This is the preferred method, as it lets you directly suggest your edits and allows our content editors to review, tweak, and merge your request. This requires a GitLab account and familiarity with Git mechanics, but don't worry, it's not as hard as it sounds!
- **[Open an issue](https://gitlab.com/dial/principles/sustainability-guide/-/issues)**  here on this repository. This does require a GitLab account.
- **Send an email** to [this very long email address](mailto:contact-project+dial-principles-sustainability-guide-31378041-issue-@incoming.gitlab.com) with details about your requested edits. This does not require a GitLab account.

## License

See the [LICENSE](LICENSE) file for the terms currently governing this content.